[TOC]

# Overview #
In this assignment, we will develop a simple cloud service called **MultiCloud** that stitches together multiple cloud platforms and, in the process, learn about the differences between different public cloud platforms, get some experience deploying popular applications, and appreciate the challenges involved in deploying multi-tenant cloud services.

Unix is a prerequisite, unless you can write simple shell scripts in your non-unix favorite OS.

### High-level goals ###
You will play the role of a software-as-a-service (SaaS) cloud service provider. You are a vendor of MultiCloud, a cloud-based service whose selling point is that it stitches together multiple cloud platforms for improved availability, performance, and lower cost for its customers. You can pick the service of your choice here. For example, you could host a simple database such as Cassandra that you are familiar with or others like MongoDB or Redis or non-database applications, preferably stateful ones. If you like, you can get fancier with, say, a Node.js or apache front-end and a database back-end or pick other client-server system that you have been meaning to learn how to use and use this assignment as an excuse to actually do so!

For this assignment, it is not important what service you choose and it is not important that you write client code to send requests to the service. But you need to host your own web service, and learn how to use MultiCloud to reconfigure your server locations to get a better performance. 

MultiCloud should be able to programmatically and dynamically create new instances on EC2 or GCE based on both observed load and current spot prices, and ensure that client requests automatically get redirected to a suitable server; for the last part we need to fiddle with DNS, which we will do with Route 53, an AWS request redirection service that can work with both AWS and GCE addresses.

The above goal is simple to state and our auto-scaling, geo-elastic MultiCloud system is quite “real”, yet it is relatively simple to achieve programmatically using standard cloud APIs. We just need to familiarize ourselves with these APIs.

# Background reading and setup #

You will need two different cloud platforms to host the Multicloud SaaS, for which the following pointers to AWS and GCP would be useful assuming you are more familiar with AWS. We will be using AWS as one of the platforms in this assignment, but for the other, you are welcome to use a different cloud provider like Azure or others, provided you can navigate that environment largely on your own.

1. Learn how to [map AWS services to corresponding services on Google Cloud Platform](https://cloud.google.com/free-trial/docs/map-aws-google-cloud-platform):  
2. Create a [Google Cloud](https://cloud.google.com/) account if you don’t already have one. Everything in this assignment will comfortably fit within the free credits that come with a new account. If you have exhausted the free credits, talk to the instructor, or just use a different cloud provider.
3. For AWS, you don’t need to use your credits as we will provide you use access keys to use.
4. If you are unfamiliar with [Google Compute Engine (GCE)](https://cloud.google.com/compute), complete a couple of the quick (<15 mins) tutorials that show you, for example, how to set up a node.js front-end, or others. They are really simple tutorials that will make you feel powerful (and perhaps also a little bit silly!).

# AWS EC2 API tools test-drive (1.5 hours)#
Follow the EC2 API tools setup and familiarization instructions [here](https://bitbucket.org/distrsys/multicloud/src/master/tutorial_EC2_API.md).

# AWS route53 DNS (30 mins)#
Familiarize yourself with AWS’ Route53 DNS service through the tutorial [here](https://bitbucket.org/distrsys/multicloud/src/master/tutorial_route53.md).

# Google Cloud API tools (1 hour)#
Familiarize yourself with how to manage Google Cloud instances [here](https://bitbucket.org/distrsys/multicloud/src/master/tutorial_GCloud_API.md).

# MultiCloud geo-elastic load balancing [6-12 hours] #

Your goal is to demonstrate MultiCloud by writing two programs, a **Loader Client** and an **Elastic Server Controller**, also simply referred to as *loader* and *controller* respectively. These are separate from the VM instances running your service itself. Here is what your loader and controller should demonstrate:

1. You should start with exactly one EC2 server instance running your service.

2. The loader should increase the load (requests/second) from zero to a sufficiently high load value until the single server instance’s CPU utilization exceeds 50%, at which point, the controller should spawn one more EC2 instance. At this point, each server’s utilization should go below 50%.

	1. Note that the controller will need to modify route53 entries to load balance the loader’s requests across the two server instances.

	2. DNS entries may take some time to reflect, so make sure your route53 TTL is 60 seconds or less and your loader changes its load slowly on timescales of a minute or more, not seconds.

3. The loader should further increase the load until each server’s utilization exceeds 75%, at which point, it should spawn a third instance on Google Cloud and update route53 entries accordingly so that each of the two EC2 server instances’ load goes below 75%.

4. The loader should then reduce its load until each EC2 instance’s load dips below 50% at which point it should terminate the Google Cloud instance.

5. The loader should further reduce its load until each of the two EC2 server instances’ load dips below 25% at which point it should switch off the second EC2 instance.

# Submission instructions #
You need to submit your code as a set of shell scripts as enumerated below, and output text files showing what successful execution looks like; below "local" means your local machine or our autograder docker.

1. `start.sh`: runs locally to create the first (entry) EC2 server instance and copy any necessary files to that instance, e.g., `controller.sh`, so that the entry instance can intelligently spawn and terminate its peers.

2. `loader.sh`: runs locally to starts a loader client that can programmatically increase the load by taking the target CPU utilization as an argument.

3. `controller.sh`: runs on the entry EC2 instance and monitors the load statistics so as to spawn a new instance if needed.

4. `update.sh`: runs locally to interact with route53 to update DNS records as instances are created or destroyed.

5. `monitor.sh`: runs locally to monitor load statistics on an instance and generate output files storing the statistics.

6. `cleanup.sh`: runs locally to terminate the initially-spawn entry EC2 instance.

7. Include the loader and controller’s sample output showing that they are behaving as expected in output text files called `loader.out` and `controller.out`.

Do not impose any directory structure on the above top-level scripts.

The above scripts can internally call a program in any language you want if useful.



DONTs: Do NOT 

	1. use instances larger than micro and small respectively on EC2 and Google; 
	
	2. use more than 2 instances on EC2;  
	
	3. use more than 3 instances total across EC2 and Google.

# Tips etc. #

[Tips page](tips.md)
____





