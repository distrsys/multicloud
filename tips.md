### Tips: ###
1. The repository has a package called `http_client` that has a simple Java HTTP client that sends simple HTTP requests. You can add multi-threading to it build your loader. The client by default will send HTTP request to a localhost web server, if you have a local web server running, and it has been configured correctly, you should see the output like:

	`Request Content:{"Cloud":"AWS","Id":750958253226714556,"Location":"us-east-1"}
Successfully send HTTP POST request with the content {"Cloud":"AWS","Id":750958253226714556,"Location":"us-east-1"} to the server http://localhost/`

2. You can also write your own loader from scratch in any language and framework and ignore the HTTP client above.

3. Your web service could be anything, for example, it could simply log and echo back a request; or do some compute and/or update something in a local database. 

4. Remember to claim and use only your access keys, otherwise you might inadvertently update someone else’s DNS entries.

(to be updated as needed.)