[TOC]

# EC2 API tools setup [30 mins]#

1. Before setting up your environment, you need to create an access key for your AWS account. Log into your AWS console and at the top right below your account name, follow `My Security Credentials → Access Keys → Create New Access Key`, then download your access key file `rootkey.csv`. 

2. Read up an introduction to AWS Command Line Interface(CLI): What is the AWS Command Line Interface? Then install AWS CLI by following Installing the AWS Command Line Interface based on your operating system. Test your installation using

    `$ aws2 --version`

3. Then configure your AWS CLI as:

    `$ aws2 configure`

    Enter your AWS Access Key ID and AWS Secret Access Key based on your `rootkey.csv`. Choose your default region name as us-east-1, and default output format as either `json` or `text`. The output snippets below use text for ease of formatting here but json is probably easier to read.

4. At this point, you should be able to test your CLI are working correctly as follows:

    `$aws2 ec2 describe-regions`

    You should see (text) output like below:

```
    REGIONS	ec2.ap-south-1.amazonaws.com	ap-south-1
    REGIONS	ec2.eu-west-2.amazonaws.com		eu-west-2
    REGIONS	ec2.eu-west-1.amazonaws.com		eu-west-1
    REGIONS	ec2.ap-northeast-2.amazonaws.com	ap-northeast-2
    REGIONS	ec2.ap-northeast-1.amazonaws.com	ap-northeast-1
    REGIONS	ec2.sa-east-1.amazonaws.com		sa-east-1
    REGIONS	ec2.ca-central-1.amazonaws.com	ca-central-1
    REGIONS	ec2.ap-southeast-1.amazonaws.com	ap-southeast-1
    REGIONS	ec2.ap-southeast-2.amazonaws.com	ap-southeast-2
    REGIONS	ec2.eu-central-1.amazonaws.com	eu-central-1
    REGIONS	ec2.us-east-1.amazonaws.com		us-east-1
    REGIONS	ec2.us-east-2.amazonaws.com		us-east-2
    REGIONS	ec2.us-west-1.amazonaws.com		us-west-1
    REGIONS	ec2.us-west-2.amazonaws.com		us-west-2
```



# Playing witht the API [1 hour]#

## Create instance ##
One can create an EC2 instance by specifying an `ami-id`. But before you create an instance you need to know what kind of AMI to use as follows.

1. Find the right AMI (Amazon provides several default machine images that you can use to start your server; you can also create your own images as we will see in a later step). The following command lists AMIs you can launch. Specifying all instead of self lists public AMIs.

    `$ aws2 ec2 describe-images --executable-users self`
   
You should see output listing `ImageId` and other information for available images.

2. Let’s use say the AMI with ID `ami-04b9e92b5572fa0d1` (Ubuntu Server 18.0.4 LTS) to create the instance; when creating a server, EC2 allows you to specify a security policy to indicate which ports are open, which ones are blocked, etc. For now I will use a security group (sg-2bf6c353) that allows all ports. Do not create instances bigger than micro on EC2 and small on Google as every time an instance is created the account is charged (even if it is immediately terminated).

    `$ aws2 ec2 run-instances --image-id ami-04b9e92b5572fa0d1 --key-name EC2 --instance-type t2.nano --security-group-ids sg-6dc31a0a --subnet-id subnet-04e2aa72  --count 1`
    
    You should see (text) output like below:
_______    
```
    375980007699    r-08be035062b88727d
    INSTANCES       0       x86_64          False   xen     ami-04b9e92b5572fa0d1   i-0ec205aa95bd83f8d     t2.nano av      
        2019-12-04T21:37:41+00:00       ip-172-30-4-164.ec2.internal    172.30.4.164            /dev/sda1       ebs     True            
        subnet-04e2aa72 hvm     vpc-44f3eb21
    CAPACITYRESERVATIONSPECIFICATION        open
    CPUOPTIONS      1       1
    MONITORING      disabled
    NETWORKINTERFACES               interface       0a:e9:2a:cf:1c:a3       eni-0670220a5bc341474   375980007699    172.30.4.164    
        True    in-use      subnet-04e2aa72 vpc-44f3eb21
    ATTACHMENT      2019-12-04T21:37:41+00:00       eni-attach-0a4cc1004526bbc13    True    0       
    attaching
    GROUPS  sg-1ec01979     default
    PRIVATEIPADDRESSES      True    172.30.4.164
    PLACEMENT       us-east-1a              default
    SECURITYGROUPS  sg-1ec01979     default
    STATE   0       pending
    STATEREASON     pending pending
```
______

        Output notes:
    
    - The string `i-0ec205aa95bd83f8d` is the instance-id we will use in the next step.
    
    - The command above uses a key called EC2 for which you should have a `EC2.pem` file in `~/.ssh`. To find the name of your key, go to your AWS console → EC2 → Key Pairs. You may need to create a key-pair if you haven’t created one.
    - You may need to specify your own subnet-id and security-group-ids. You can look them up or create them here and here respectively.
    
    
3. Check whether the instance is up using ec2-describe-instances with the just-created instance-id and check that it is up and running with a public IP address that will be listed as shown below:

    `$aws2 ec2 describe-instances --instance-ids i-0ec205aa95bd83f8d`
    
    You should see output like below:
______
```
RESERVATIONS    375980007699    r-08be035062b88727d
INSTANCES       0       x86_64          False   True    xen     ami-04b9e92b5572fa0d1
   i-0ec205aa95bd83f8d     t2.nano av      2019-12-04T21:37:41+00:00       ip-172-30-4-164.ec2.internal    172.30.4.164            
       54.242.195.247  /dev/sda1       ebs     True            subnet-04e2aa72 hvm     vpc-44f3eb21
BLOCKDEVICEMAPPINGS     /dev/sda1
EBS     2019-12-04T21:37:41+00:00       True    attached        vol-08ebcbae936cca3db
CAPACITYRESERVATIONSPECIFICATION        open
CPUOPTIONS      1       1
HIBERNATIONOPTIONS      False
MONITORING      disabled
NETWORKINTERFACES               interface       0a:e9:2a:cf:1c:a3       eni-0670220a5bc341474   375980007699    172.30.4.164   
    True    in-use  subnet-04e2aa72 vpc-44f3eb21
ASSOCIATION     amazon          54.242.195.247
ATTACHMENT      2019-12-04T21:37:41+00:00       eni-attach-0a4cc1004526bbc13    True
    0       attached
GROUPS  sg-1ec01979     default
PRIVATEIPADDRESSES      True    172.30.4.164
ASSOCIATION     amazon          54.242.195.247
PLACEMENT       us-east-1a              default
SECURITYGROUPS  sg-1ec01979     default
STATE   16      running

```
______

    If you see a name like `ec2-54-242-195-247.compute-1.amazonaws.com` instead of an IP address in bold above, you can get the public DNS name corresponding to the above public IP using either `nslookup` or `dig`.
    
    
    
## ssh into an instance ##

    `ssh -i ~/.ssh/av.pem ubuntu@54.242.195.247`
    
    or
    
    `ssh -i ~/.ssh/EC2.pem ubuntu@ec2-54-242-195-247.compute-1.amazonaws.com`
    
Note that the default user name may be something else, e.g., `ec2-user` for Amazon Linux, depending on the instance type you choose to create.

## Load up your instance (literally) ##

Install whatever service you want on your instance (refer to the quickstart pages for popular key-value stores in the previous section or pick your own).

As a simpler test of load monitoring, run the following script inside your instance to make your poor instance use up 100% of its CPU just because you commanded so:

`$ while [[ true ]]; do product=3754127*573211; done &`

Check using `top` that the `t2.nano` core is indeed blazing at near-full utilization multiplying the silly numbers above. Kill the above script (`kill -9 process_id`).



## Monitor load statistics on your instance ##

1. You can find out what metrics you can monitor using

    `$ aws2 cloudwatch list-metrics --namespace "AWS/EC2" --dimensions Name=InstanceId,Value=i-0ec205aa95bd83f8d`
    
    You should see output like below:
    ```
    METRICS	DiskWriteBytes	AWS/EC2
    DIMENSIONS	InstanceId	i-082d5d27c48780ae1
    METRICS	DiskReadOps	AWS/EC2
    DIMENSIONS	InstanceId	i-082d5d27c48780ae1
    METRICS	CPUCreditBalance	AWS/EC2
    DIMENSIONS	InstanceId	i-082d5d27c48780ae1
    ```
    
2. Before actually monitoring the instance, you will need to enable monitoring:

    `$ aws ec2 monitor-instances --instance-id i-0ec205aa95bd83f8d`
    
    You should see output like below:
    ```
    INSTANCEMONITORINGS	i-082d5d27c48780ae1
    MONITORING	pending
    ```
    
3. We are finally ready to monitor our instance. You can of course also simply do “ssh instanceIP top” to run top on the remote host with the appropriate option to get resource utilization metrics. But the CloudWatch APIs already do this for a whole bunch of metrics, so it is better to use it. Let’s get the CPU utilization for the last half an hour:

    `$aws2 cloudwatch get-metric-statistics --namespace "AWS/EC2" --dimensions Name=InstanceId,Value=i-0ec205aa95bd83f8d --metric-name CPUUtilization --period 60 --statistics "Average" --start-time $(($(date +%s)-1800)) --end-time $(date +%s)`
    
    You should see output like below:
____
```
CPUUtilization
DATAPOINTS      5.9     2019-12-04T21:57:00+00:00       Percent
DATAPOINTS      0.6661202185792342      2019-12-04T21:52:00+00:00       Percent
DATAPOINTS      0.0     2019-12-04T22:05:00+00:00       Percent
DATAPOINTS      0.0     2019-12-04T21:47:00+00:00       Percent
DATAPOINTS      0.033898305084745       2019-12-04T21:42:00+00:00       Percent
DATAPOINTS      0.0     2019-12-04T22:04:00+00:00       Percent
DATAPOINTS      6.610169491525425       2019-12-04T21:37:00+00:00       Percent
DATAPOINTS      0.163934426229511       2019-12-04T22:03:00+00:00       Percent
```
____

Note that the “date +%s” command will generate the UNIX timestamp for you.

Now start your web service, stress test it, and verify that you can get CPU utilization stats as expected.


## Create an image ##

Take a snapshot of a running instance as follows:

`$ aws2 ec2 create-image --instance-id i-0ec205aa95bd83f8d --name image-1 --no-reboot`

You should see output like below:

`$ ami-0d384a3b9c71b156e`

Verify that the image is actually created:

`$ aws2 ec2 describe-images --owner self --image-ids ami-0d384a3b9c71b156e`

You should see output like below:
_____
```
IMAGES  x86_64  2019-12-04T22:10:03.000Z        True    xen     ami-0d384a3b9c71b156e
   375980007699/image-1    machine image-1 375980007699    False   /dev/sda1       ebs     simple  available       hvm
BLOCKDEVICEMAPPINGS     /dev/sda1       
EBS     True    False   snap-0c850232498d446bb  8       gp2
BLOCKDEVICEMAPPINGS     /dev/sdb        ephemeral0
BLOCKDEVICEMAPPINGS     /dev/sdc        ephemeral1
```
_____

Note: Creating an image is a one-time activity in this assignment, so you can also just do it using the console. But if you decide to change your application, the command-line API might be quicker to script up in a simple shell script file.

## Launch the image ##

The reason we created the image was to be able to launch it anytime we want, which we can do as follows:

`$ aws2 ec2 run-instances --image-id ami-0d384a3b9c71b156e --key-name EC2 --instance-type t2.nano --security-group-ids sg-2bf6c353 --count 1`


You should see output like below:

____
```
015406598032	r-082cc53256747064e
INSTANCES	0	x86_64		False	xen	ami-4d2b4a37	 i-0ee305d872deebc9d	t2.nano	EC2	2017-12-05T18:57:12.000Z	ip-172-31-0-8.ec2.internal	172.31.0.8		/dev/xvda	ebs	True		subnet-cd4a34bb	hvm	vpc-399c8e5d
```
____



##  Terminate  instances ##

Kill both instances we created above as:

`$ aws ec2 terminate-instances --instance-ids i-0ec205aa95bd83f8d i-0ee305d872deebc9d`

You should see output like below:
____
```
TERMINATINGINSTANCES	i-0ec205aa95bd83f8d
CURRENTSTATE	32	shutting-down
PREVIOUSSTATE	16	running
TERMINATINGINSTANCES	i-0ee305d872deebc9d
CURRENTSTATE	32	shutting-down
PREVIOUSSTATE	16	running
```
____

Great! Now you know how to programmatically launch an image instance and terminate it, and you know how to increase load on your instance and programmatically monitor that load using Cloudwatch.

