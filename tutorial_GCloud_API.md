[TOC]

# Create Google Cloud instances #
Learn to programmatically launch a snapshotted Google Cloud instance just like EC2 from here:
https://cloud.google.com/sdk/docs/quickstarts
Google also has a web-based shell called Google Cloud Shell, which you can interact with Google Cloud directly without installing the Cloud SDK. 

## Install Google Cloud SDK ##
1. We assume you are using Linux; if so, download and install Google Cloud SDK

____
```
$ wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-182.0.0-linux-x86_64.tar.gz
$ tar zxf google-cloud-sdk-182.0.0-linux-x86_64.tar.gz
$ ./google-cloud-sdk/install.sh
$ export PATH=$PATH:$HOME/google-cloud-sdk/bin
```
____

	Add the above export to your `~/.bashrc` or `~/.bash_profile` shell initialization script. Include the path to your google-cloud-sdk in your system path if your google-cloud-sdk is in your $HOME. 

	`export PATH=$PATH:$HOME/google-cloud-sdk/bin`

	Then, restart your terminal for the changes to take effect.

2.  Initialize gcloud as: 

	`$ gcloud init`

In the first step, choose 2 for `[2] Log in with a new account`, then copy and paste the link in your browser to get authenticated. Once authenticated in your browser successfully, you need to copy and paste the generated code back to your application. Follow the [quickstart instructions](https://cloud.google.com/sdk/docs/quickstarts) to finish the rest of the initialization. 

3. Verify that your account has been setup properly by the Cloud SDK tools:

`$ gcloud auth list`

You should see output like below:
____
```
You should see output like below:
Credentialed Accounts
ACTIVE  ACCOUNT
*       gaozy@cs.umass.edu
```
____

If you haven’t yet applied a public key to your Cloud Platform Console project or instance, please follow [Manage Instance Access with SSH Keys](https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys) to set up your ssh key. You need to specify your username when creating your ssh key, the username will be used to ssh into your instance.

## Create Google Cloud instance ##
One can create a Google instance by specifying an image id. Similar with AWS, before you create an instance you need to know what kind of image to use. This is how you would do it

`$ gcloud compute images list`

The output should look like(trimmed):

```
NAME                 PROJECT            FAMILY                DEPRECATED  STATUS
centos-6-v20171129   centos-cloud       centos-6               READY
centos-7-v20171129   centos-cloud       centos-7               READY
...
```

We will use the image with name `cos-stable-62-9901-79-0` to create the first instance, when specifying image name to create new instance, you also need to provide its image project name. When creating a server, Google allows you to specify a firewall rule to indicate which ports are open, which ones are blocked, etc. For now I will use a firewall rule (all-traffic) that allows all traffic from all ports. To create your own firewall rule, log into your console → VPC network → Firewall rules → Create Firewall Rule. Following command shows you how to create a g1-small compute instance in the zone us-east1-b

`$ gcloud compute instances create instance-0 --machine-type g1-small --image cos-stable-62-9901-79-0 --image-project cos-cloud --tags all-traffic --zone us-east1-b`

The output should look like:
____
```
Created [https://www.googleapis.com/compute/v1/projects/liquid-layout-188101/zones/us-east1-b/instances/instance-0].
NAME        ZONE       MACHINE_TYPE  INTERNAL_IP  EXTERNAL_IP     STATUS
instance-0  us-east1-b g1-small      10.142.0.3   35.196.133.194  RUNNING
```
____

Once it returns, it means the instance is ready to use, so you don’t need to check the instance state to make sure it’s ready to use (unlike the asynchronous instance creation API with AWS). 

## ssh into your Google Cloud instance ##
Suppose the username of the key Google is gaozy, use the public IP to directly ssh into your Google instance:

`$ ssh -i ~/.ssh/Google gaozy@35.196.133.194`

You could also use gcloud to ssh into your instance without setting up a key:

`$ gcloud compute ssh "instance-0"`

Then start to deploy your web application on the instance like what you did for AWS.

## Monitor your Google Cloud instance ##

In this assignment, you are not required to monitor Google Compute Platform as its API seems harder to use. Here are a few links for you to get some knowledge on monitoring Google Cloud if interested.

Google uses a metrics explorer called Stackdriver to monitor its service. To create an account:
https://app.google.stackdriver.com/account/login/?next=/

Here is the GCP metric list that can be monitored with Stackdriver:
https://cloud.google.com/monitoring/api/metrics_gcp

## Create an image ##
To create an image from your running instance:

`$ gcloud compute images create instance-0-image --source-disk instance-0 --force`

The output should look like:

```
Created [https://www.googleapis.com/compute/v1/projects/liquid-layout-188101/global/images/instance-0-image].
WARNING: Some requests generated warnings:
 - 
NAME              PROJECT               FAMILY  DEPRECATED  STATUS
instance-0-image  liquid-layout-188101                      READY
```

## Launch the image ##
Launch a new instance from the one you created:

`$ gcloud compute instances create instance-1 --machine-type g1-small --image instance-0-image --image-project liquid-layout-188101 --tags all-traffic --zone us-east1-b`

You should see output like:

```
Created [https://www.googleapis.com/compute/v1/projects/liquid-layout-188101/zones/us-east1-b/instances/instance-1].
NAME     ZONE    MACHINE_TYPE  PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP    STATUS
instance-1  us-east1-b  g1-small   10.142.0.2   35.196.56.199  RUNNING
```

## Stop or delete instances ##
Delete the created instances:

`$ gcloud compute instances delete instance-0 instance-1`



