# Using route53 DNS #
We have set up a domain name `mymulticloud.org` on Route 53, you get a subdomain and set up an A record for the subdomain, e.g., `578.mymulticloud.org` where `578` is my user ID as well as my subdomain name. You need to setup a user profile with your AWS CLI. We provide you an IAM user, which can only access to route53 service under our AWS account. There are a number of pre-created IAM user IDS posted [here](https://docs.google.com/spreadsheets/d/1plj69xYHT9a9WrRcqbMsOstARfz2TB2LX8zb2tgEr50/edit?usp=sharing). Claim your ID -- you just need one -- and comment on the spreadsheet accordingly. 


To setup your user profile say `user2`:

`$aws configure --profile user2`

Enter the AWS Key Id and AWS Secret Key you claimed.

Your service clients in this example will use the name `user2.mymulticloud.org` to access your service. They should reasonably not be bothered to keep track of changing IP addresses from where they get their service. For this, we need to tell the Internet’s DNS how to find your service. To this end, we will use Route 53 as follows

`$ aws route53 change-resource-record-sets --hosted-zone-id Z3RQJ06I7DNHJH --change-batch "$(cat /Users/arun/create_record.json)" --profile user2`

so as to produce output like below:

```
CHANGEINFO	A new record set for the zone.	/change/CQWC31Z7XRIB6	PENDING	2017-12-05T19:07:51.469Z
```

where `/Users/arun/create_record.json` is my path to the file, you should replace it with yours. We have pre-created a json file create_record.json with the following sample content:

```
{
  "Comment": "A new record set for the zone.",
  "Changes": [
        {
        "Action": "CREATE",
        "ResourceRecordSet": {
        "Name": "578.mymulticloud.org.",
        "Type": "A",
        "SetIdentifier":"North America",
        "GeoLocation": {
                "ContinentCode": "NA"
        },
        "TTL": 0,
        "ResourceRecords": [
        {
                "Value": "54.88.152.175"
        }
        ]
        }
        }
  ]
}
```

This will create an A record with IP address 54.88.152.175, and we use Geolocation Routing Policy for this record. To update it in the future, change the “Action” in the json file to “UPSERT”. Figure out how to use the random or round-robin load balancing policy. The subdomain must be in the format of `user_id.mymulticloud.org`, where the `user_id` is the one assigned to you. You could set the “Value” in the “ResourceRecords” to the IP address of the instance you created. You can try to resolve the subdomain name with `nslookup` or `dig`. It may take some time for the record to begin to resolve correctly. More details on Route 53 API can be found here or at AWS pages. 


Please take care not to change the IPs of other students’ services. Also, set the TTL to no more than a minute, otherwise you will not be able to change it quickly (because of DNS’s caching).
Note that route53 can take any IP addresses including IP addresses of instances created on other clouds such as Google’s.
